# :beers: Emoji Substitutions

Ever want Slack style emjoi entry in other macOS applications? Now you can simply type `:bell:` and get a :bell:.

## :inbox_tray: Installation

1. Download [Text Substituations.plist](raw/master/Text%20Substitutions.plist?inline=false) from this repository.
2. Drag it onto the Text Substitutions pane in the Keyboard Control Panel.
![Keyboard Control Panel with emoji substitutions](images/keyboard-text-emoji.png)
3. Type your emoji in your favorite app.

![TextEdit with :alarm_clock:](images/textedit-example.png)

## :bangbang: Issues

* Until the full emjoi substitution string is typed, macOS won't prompt to auto-complete the substitutions. I haven't found a way around this yet.

I am sure there are missing emoji as I used an automated script to generate the plist from a random website that was semi-parsable. If so, [file and issue](issues/new) or submit a pull request.